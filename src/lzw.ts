class LZW {
	static compress(uncompressed: string): string {
		const UNCOMPRESSED_LENGTH = uncompressed.length;

		let dictionary: {[key: string]: number} = {};
		let dictionarySize: number = 256;
		let word: string = "";
		let digitizedString: number[] = [];

		for (let i = 0; i < dictionarySize; i++) {
			dictionary[String.fromCharCode(i)] = i;
		}

		for (let i = 0; i < UNCOMPRESSED_LENGTH; i++) {
			const currentChar = uncompressed[i];

			if (currentChar.charCodeAt(0) > 256) {
				throw Error("Unsuported char.");
			}

			const potentialWord = word + currentChar;

			if (dictionary.hasOwnProperty(potentialWord)) {
				word = potentialWord;
			} else {
				digitizedString.push(dictionary[word]);
				dictionary[potentialWord] = dictionarySize++;
				word = currentChar;
			}
		}

		if (word !== "") {
			digitizedString.push(dictionary[word]);
		}

		let result: string = String.fromCharCode(...digitizedString);

		return result;
	}

	static decompress(compressed: string): string {
		const COMPRESSED_LENGTH = compressed.length;

		if (COMPRESSED_LENGTH === 0) {
			return "";
		}

		let dictionary: {[key: number]: string} = {};
		let dictionarySize: number = 256;
		let word: string = compressed[0];
		let result: string = word;
		let entry: string = "";

		for (let i = 0; i < dictionarySize; i++) {
			dictionary[i] = String.fromCharCode(i);
		}

		for (let i = 1; i < COMPRESSED_LENGTH; i++) {
			const currentNumber = compressed[i].charCodeAt(0);

			if (dictionary[currentNumber]) {
				entry = dictionary[currentNumber];
			} else {
				if (currentNumber === dictionarySize) {
					entry = word + word[0];
				} else {
					throw Error("Error in processing");
				}
			}

			result+= entry;

			dictionary[dictionarySize++] = word + entry[0];

			word = entry;
		}

		return result;
	}

}

const doButton = document.getElementById("do") as HTMLInputElement;
const undoButton = document.getElementById("undo") as HTMLInputElement;
const textarea = document.getElementById("src") as HTMLTextAreaElement;
const div = document.getElementById("dist") as HTMLDivElement;

let encoded: string = "";

doButton.onclick = () => {
	encoded = LZW.compress(textarea.value);
	div.innerText = encoded;
};

undoButton.onclick = () => {
	div.innerText = LZW.decompress(encoded);
};
